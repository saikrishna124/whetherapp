
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ActivityIndicator, Platform, PermissionsAndroid, Image, TouchableOpacity } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { styles } from '../styles/styles';
import { Constants } from '../utils/Constants';
import AppColors from '../utils/AppColors';

const API_KEY = 'ecd289975bcc0a77596d4d63cc71a3c3'; // Replace with your own API key

interface WeatherData {
  main: {
    temp: number;
    humidity: number;
  };
  wind: {
    speed: number;
  };
  weather: {
    description: string;
    icon: string;
  }[];
}

const WeatherScreen: React.FC = () => {
    const [location, setLocation] = useState<any | null>(null);
    const [weatherData, setWeatherData] = useState<WeatherData | null>(null);
    const [manualLocation, setManualLocation] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    useEffect(() => {
      setLoading(true);
      requestLocationPermission();
    }, []);
  
    const loadCachedWeatherData = async () => {
      try {
        const cachedData = await AsyncStorage.getItem('weatherData');
        if (cachedData !== null) {
          setWeatherData(JSON.parse(cachedData));
        }
      } catch (error) {
        console.error('Error loading cached weather data:', error);
      }
    };
  
  
    const fetchCurrentLocation = () => {
      Geolocation.getCurrentPosition(
        (position:any) => {
          setLocation(position.coords);
          setLoading(false); // Move setLoading inside the success callback
        },
        (error:any) => {
          console.error(error);
          setError('Error fetching location');
          setLoading(false); // Move setLoading inside the error callback
        },
        { enableHighAccuracy: true } // Example options
      );
    };
  
  
    const requestLocationPermission = async () => {
      if (Platform.OS === 'android') {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Permission',
              message: 'This app needs access to your location to provide weather information.',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('Location permission granted');
            fetchCurrentLocation();
          } else {
            console.log('Location permission denied');
            setError('Location permission denied');
            setLoading(false);
          }
        } catch (err) {
          console.error(err);
          setError('Error fetching location');
          setLoading(false);
        }
      } else {
        // For iOS, no additional permission is needed
        fetchCurrentLocation();
      }
    };
  
  
    const cacheWeatherData = async (data: WeatherData) => {
      try {
        await AsyncStorage.setItem('weatherData', JSON.stringify(data));
      } catch (error) {
        console.error('Error caching weather data:', error);
      }
    };
  
    const fetchWeatherData = async () => {
      setLoading(true);
      setError('');
  
      try {
        let response = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${location?.latitude}&lon=${location?.longitude}&appid=${API_KEY}&units=metric`);
        let data: WeatherData = await response.json();
        setWeatherData(data);
        cacheWeatherData(data); // Cache the fetched data
        
      } catch (err) {
        console.error(err);
        setError('Error fetching weather data');
        loadCachedWeatherData(); // Load cached data when fetching fails
      }
  
      setLoading(false);
    };
  
    const fetchWeatherByManualLocation = async () => {
      setLoading(true);
      setError('');
  
      try {
        let response = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${manualLocation}&appid=${API_KEY}&units=metric`);
        let data: WeatherData = await response.json();
        setWeatherData(data);
        cacheWeatherData(data); // Cache the fetched data
        console.log("weatherData",weatherData)
      } catch (err) {
        console.error(err);
        setError('Error fetching weather data');
        loadCachedWeatherData(); // Load cached data when fetching fails
  
      }
  
      setLoading(false);
    };
  
  
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{Constants.APP_TITLE}</Text>
        
        {loading ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
          <View>
            {error ? (
              <Text style={styles.error}>{error}</Text>
            ) : (
              <View>
                {location && (
                  <View>
                    <Text style={styles.location}>Latitude: {location.latitude}</Text>
                    <Text style={styles.location}>Longitude: {location.longitude}</Text>

                    <TouchableOpacity  onPress={fetchWeatherData} style={[styles.input,{justifyContent:'center',alignItems:'center'}]}>
                  <Text style={{paddingHorizontal:10,color:AppColors.color_blue,fontSize:16,fontWeight:'bold'}}>{Constants.GET_WEATHER}</Text>
                </TouchableOpacity>
                  </View>
                )}
  
                <TextInput
                  style={styles.input}
                  placeholder={Constants.ENTER_LOCATION}
                  value={manualLocation}
                  onChangeText={(text) => setManualLocation(text)}
                />
                <TouchableOpacity  onPress={fetchWeatherByManualLocation} style={[styles.input,{justifyContent:'center',alignItems:'center'}]}>
                  <Text style={{paddingHorizontal:10,color:AppColors.color_blue,fontSize:16,fontWeight:'bold'}}>{Constants.GET_WETHER_BY_LOCATION}</Text>
                </TouchableOpacity>
                {/* <Button title="Get Weather by Location"/> */}
              </View>
            )}
  
         {weatherData && (
              <View>
                <Text style={styles.location}>Temperature: {weatherData.main.temp} °C</Text>
                {weatherData.weather.map((item, index) => (
                  <View key={index} style={styles.weatherRow}>
                    <Text style={styles.weatherDescription}>{item.description}</Text>
                    <Image
                      style={styles.weatherIcon}
                      source={{ uri: `http://openweathermap.org/img/w/${item.icon}.png` }}
                      resizeMode='contain'
                    />
                  </View>
                ))}
              </View>
            )}
            <View style={{height:1,backgroundColor:AppColors.color_white,marginVertical:10,marginHorizontal:15}}>
              </View>
            {weatherData && (
              <View style={{}}>
                <Text style={styles.location}>{Constants.HUMIDITY} {weatherData.main.humidity}%</Text>
                <Text style={styles.location}>{Constants.WIND_SPEED} {weatherData.wind.speed} m/s</Text>
              </View>
            )}
            {/* temparature */}
          </View>
        )}
      </View>
    );
  }
  
export default WeatherScreen;
