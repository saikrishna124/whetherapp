import { StyleSheet } from 'react-native';
import AppColors from '../utils/AppColors';

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#3EB489',
    //   alignItems: 'center',
    //   justifyContent: 'center',
    },
    title: {
      fontSize: 24,
      marginBottom: 20,
      textAlign:'center',
      paddingVertical:10,
      color:AppColors.color_white
    },
    input: {
      height: 50,
      borderColor: 'gray',
      backgroundColor:AppColors.color_white,
      marginHorizontal:10,
      borderRadius:10,
      paddingHorizontal:5,
      marginBottom: 20,
      elevation:5
    },
    location: {
      fontSize: 18,
      paddingHorizontal:15,
      color:AppColors.color_white,
      paddingVertical:5,
      marginBottom: 10,
    },
    weatherText: {
      fontSize: 16,

      marginBottom: 10,
    },
    weatherRow: {
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal:15,
      marginBottom: 10,
    },
    weatherDescription: {
      flex: 1,
      marginRight: 10,
      fontSize:18,
      color:AppColors.color_white
    },
    weatherIcon: {
      width: 50,
      height: 50,

    },
    error: {
      color: 'red',
      marginBottom: 20,
    },
  });
