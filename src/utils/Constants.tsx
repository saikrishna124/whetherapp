export const Constants = {
    APP_TITLE: 'Weather App',
    CURRENT_LOCATION: 'Current Location:',
    ENTER_MANUAL_LOCATION: 'Enter manual location',
    GET_WEATHER: 'Get Current Location Weather',
    LOADING_LOCATION: 'Loading...',
    ERROR_FETCHING_LOCATION: 'Error fetching location',
    TEMPERATURE_UNIT: '°C',
    HUMIDITY: 'Humidity:',
    WIND_SPEED: 'Wind Speed:',
    ERROR_FETCHING_WEATHER: 'Error fetching weather data',
    ENTER_LOCATION:'Enter location',
    GET_WETHER_BY_LOCATION:"Get Weather by Location",
    
  };
  